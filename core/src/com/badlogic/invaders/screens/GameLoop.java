/*
 * Copyright 2010 Mario Zechner (contact@badlogicgames.com), Nathan Sweet (admin@esotericsoftware.com)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package com.badlogic.invaders.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.mappings.Ouya;
import com.badlogic.invaders.Invaders;
import com.badlogic.invaders.Renderer;
import com.badlogic.invaders.simulation.Simulation;
import com.badlogic.invaders.simulation.SimulationListener;

public class GameLoop extends InvadersScreen implements SimulationListener {
	/** the simulation **/
	private final Simulation simulation;
	/** the renderer **/
	private final Renderer renderer;
	/** explosion sound **/
	private final Sound explosion;
	/** shot sound **/
	private final Sound shot;

	/** controller **/
	private int buttonsPressed = 0;
	private ControllerListener listener = new ControllerAdapter() {
		@Override
		public boolean buttonDown(Controller controller, int buttonIndex) {
			buttonsPressed++;
			return true;
		}

		@Override
		public boolean buttonUp(Controller controller, int buttonIndex) {
			buttonsPressed--;
			return true;
		}
	};

	public GameLoop (Invaders invaders) {
		super(invaders);
		simulation = new Simulation(invaders.getInvaderFactories());
		simulation.listener = this;
		renderer = new Renderer();
		explosion = Gdx.audio.newSound(Gdx.files.internal("data/explosion.wav"));
		shot = Gdx.audio.newSound(Gdx.files.internal("data/shot.wav"));

		if (invaders.getController() != null) {
			invaders.getController().addListener(listener);
		}
	}

	@Override
	public void dispose () {
		renderer.dispose();
		shot.dispose();
		explosion.dispose();
		if (invaders.getController() != null) {
			invaders.getController().removeListener(listener);
		}
		simulation.dispose();
	}

	@Override
	public Class<? extends InvadersScreen> next() {
		if (simulation.ship.lives == 0) {
            this.invaders.updateHighScore(simulation.score);
            return GameOver.class;
        }
        return null;
	}

	@Override
	public void draw (float delta) {
		renderer.render(simulation, delta);
	}

	@Override
	public void update (float delta) {
		simulation.update(delta);

		float accelerometerY = Gdx.input.getAccelerometerY();
		if (accelerometerY < 0)
			simulation.moveShipLeft(delta, Math.abs(accelerometerY) / 10);
		else if (accelerometerY > 0)
			simulation.moveShipRight(delta, Math.abs(accelerometerY) / 10);

        boolean movedLeftOrRight = false;

		if (invaders.getController() != null) {
			if (buttonsPressed > 0) {
				simulation.shot();
			}

			// if the left stick movedLeftOrRight, move the ship
			float axisValue = invaders.getController().getAxis(Ouya.AXIS_LEFT_X) * 0.5f;
			if (Math.abs(axisValue) > 0.25f) {
				if (axisValue > 0) {
                    movedLeftOrRight = true;
					simulation.moveShipRight(delta, axisValue);
				} else if (axisValue < 0) {
                    movedLeftOrRight = true;
					simulation.moveShipLeft(delta, -axisValue);
				}
			}

            // if the left stick movedLeftOrRight, move the ship
            float zaxisValue = invaders.getController().getAxis(Ouya.AXIS_LEFT_Y) * 0.5f;
            if (Math.abs(zaxisValue) > 0.25f) {
                if (zaxisValue > 0) {
                    simulation.moveShipDown(delta, zaxisValue);
                } else if (zaxisValue < 0) {
                    simulation.moveShipUp(delta, -zaxisValue);
                }
            }
		}

		if (Gdx.input.isKeyPressed(Keys.DPAD_LEFT) || Gdx.input.isKeyPressed(Keys.A)) {
            simulation.moveShipLeft(delta, 0.5f);
            movedLeftOrRight = true;
        } else if (Gdx.input.isKeyPressed(Keys.DPAD_RIGHT) || Gdx.input.isKeyPressed(Keys.D)) {
            simulation.moveShipRight(delta, 0.5f);
            movedLeftOrRight = true;
        }

        if (!movedLeftOrRight) {
            simulation.notMovingLeftOrRight();
        }

        if (Gdx.input.isKeyPressed(Keys.DPAD_UP) || Gdx.input.isKeyPressed(Keys.W)) simulation.moveShipUp(delta, 0.5f);
        if (Gdx.input.isKeyPressed(Keys.DPAD_DOWN) || Gdx.input.isKeyPressed(Keys.S)) simulation.moveShipDown(delta, 0.5f);
		if (Gdx.input.isTouched() || Gdx.input.isKeyPressed(Keys.SPACE)) simulation.shot();
	}

	@Override
	public void explosion () {
		explosion.play();
	}

	@Override
	public void shot () {
		shot.play();
	}
}
