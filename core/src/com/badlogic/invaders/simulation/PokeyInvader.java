/*
 * Copyright 2010 Mario Zechner (contact@badlogicgames.com), Nathan Sweet (admin@esotericsoftware.com)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package com.badlogic.invaders.simulation;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;

public class PokeyInvader extends Invader {
	public PokeyInvader(Model model, float x, float y, float z) {
		super(model, x, y, z);
	}

    public float getRadius() {
        return 1.5f;
    }

    @Override
    public float getPoints() {
        return 50;
    }

    public void update (float delta, float speedMultiplier) {
        super.update(delta, speedMultiplier);

        float x = (float) Math.sin(time) * 0.06f;
        float z = time * 0.02f * speedMultiplier;
        transform.trn(x, 0, z);
	}

    public static class Factory implements InvaderFactory<PokeyInvader> {

        private Model invaderModel;
        private AssetManager assetManager;

        @Override
        public void loadAssets(AssetManager assetManager) {
            assetManager.load("data/GHAANON R1.g3db", Model.class);
            this.assetManager = assetManager;
        }

        @Override
        public void doneLoading() {
            invaderModel = assetManager.get("data/GHAANON R1.g3db");
        }

        @Override
        public PokeyInvader create(float x, float y, float z) {
            return new PokeyInvader(invaderModel, x, y, z);
        }

        @Override
        public void dispose() {
            invaderModel.dispose();
        }
    }
}
