/*
 * Copyright 2010 Mario Zechner (contact@badlogicgames.com), Nathan Sweet (admin@esotericsoftware.com)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package com.badlogic.invaders.simulation;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.UBJsonReader;
import com.badlogic.invaders.EveryXTime;
import com.badlogic.invaders.NoMoreThan;

public class Simulation implements Disposable {
	public final static float PLAYFIELD_MIN_X = -14;
	public final static float PLAYFIELD_MAX_X = 14;
	public final static float PLAYFIELD_MIN_Z = -15;
	public final static float PLAYFIELD_MAX_Z = 2;
    private final Map<Class<? extends Invader>, InvaderFactory<?>> invaderFactory;

    public ArrayList<Invader> invaders = new ArrayList<Invader>();
	public ArrayList<Block> blocks = new ArrayList<Block>();
	public ArrayList<Shot> shots = new ArrayList<Shot>();
	public ArrayList<Explosion> explosions = new ArrayList<Explosion>();
	public Ship ship;
	public List<Shot> shipShots = new ArrayList<Shot>( );
	public transient SimulationListener listener;
	public float multiplier = 1;
	public int score;
	public int wave = 1;

	public Model shipModel;
	public Model blockModel;
	public Model shotModel;
	public Model explosionModel;

	private ArrayList<Shot> removedShots = new ArrayList<Shot>();
	private ArrayList<Explosion> removedExplosions = new ArrayList<Explosion>();

	private final Vector3 tmpV1 = new Vector3();
	private final Vector3 tmpV2 = new Vector3();

    private EveryXTime invaderAdder;
    private EveryXTime levelUpper;
    private NoMoreThan canShoot;

	public Simulation (Map<Class<? extends Invader>, InvaderFactory<?>> invaderFactory) {
        this.invaderFactory = invaderFactory;
		populate();
        this.invaderAdder = new EveryXTime(1, TimeUnit.SECONDS, new Runnable() {
            @Override
            public void run() {
                addInvader(UfoInvader.class);
            }
        });
        this.levelUpper = new EveryXTime(10, TimeUnit.SECONDS, new Runnable() {
            @Override
            public void run() {
                System.out.println("Level up");
                multiplier += 0.2f;
                wave++;
                addInvader(PokeyInvader.class);
                invaderAdder = new EveryXTime(1000 - wave * 100, TimeUnit.MILLISECONDS, new Runnable() {
                    @Override
                    public void run() {
                        addInvader(UfoInvader.class);
                    }
                });
            }
        });
        this.canShoot = new NoMoreThan(200, TimeUnit.MILLISECONDS);
	}

	private void populate () {
		ObjLoader objLoader = new ObjLoader();
		shipModel = objLoader.loadModel(Gdx.files.internal("data/ship.obj"));

        System.out.println("Exists? " + new File("data/mini.g3db").exists());
		blockModel = objLoader.loadModel(Gdx.files.internal("data/block.obj"));
		shotModel = objLoader.loadModel(Gdx.files.internal("data/shot.obj"));

		final Texture shipTexture = new Texture(Gdx.files.internal("data/ship.png"), Format.RGB565, true);
		shipTexture.setFilter(TextureFilter.MipMap, TextureFilter.Linear);
		shipModel.materials.get(0).set(TextureAttribute.createDiffuse(shipTexture));

//        ((ColorAttribute)invaderModel.materials.get(0).get(ColorAttribute.Diffuse)).color.set(0, 0, 1, 0.5f);
//        invaderModel.materials.get(0).set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA));

//        invaderModel.materials.get(0).set(ColorAttribute.createDiffuse(1, 1, 0, 1f));

		((ColorAttribute)blockModel.materials.get(0).get(ColorAttribute.Diffuse)).color.set(0, 0, 1, 0.5f);
		blockModel.materials.get(0).set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA));

		shotModel.materials.get(0).set(ColorAttribute.createDiffuse(1, 1, 0, 1f));

		final Texture explosionTexture = new Texture(Gdx.files.internal("data/explode.png"), Format.RGBA4444, true);
		explosionTexture.setFilter(TextureFilter.MipMap, TextureFilter.Linear);

		final Mesh explosionMesh = new Mesh(true, 4 * 16, 6 * 16, new VertexAttribute(Usage.Position, 3, "a_position"),
			new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoord0"));

		float[] vertices = new float[4 * 16 * (3 + 2)];
		short[] indices = new short[6 * 16];
		int idx = 0;
		int index = 0;
		for (int row = 0; row < 4; row++) {
			for (int column = 0; column < 4; column++) {
				vertices[idx++] = 1;
				vertices[idx++] = 1;
				vertices[idx++] = 0;
				vertices[idx++] = 0.25f + column * 0.25f;
				vertices[idx++] = 0 + row * 0.25f;

				vertices[idx++] = -1;
				vertices[idx++] = 1;
				vertices[idx++] = 0;
				vertices[idx++] = 0 + column * 0.25f;
				vertices[idx++] = 0 + row * 0.25f;

				vertices[idx++] = -1;
				vertices[idx++] = -1;
				vertices[idx++] = 0;
				vertices[idx++] = 0f + column * 0.25f;
				vertices[idx++] = 0.25f + row * 0.25f;

				vertices[idx++] = 1;
				vertices[idx++] = -1;
				vertices[idx++] = 0;
				vertices[idx++] = 0.25f + column * 0.25f;
				vertices[idx++] = 0.25f + row * 0.25f;

				final int t = (4 * row + column) * 4;
				indices[index++] = (short)(t);
				indices[index++] = (short)(t + 1);
				indices[index++] = (short)(t + 2);
				indices[index++] = (short)(t);
				indices[index++] = (short)(t + 2);
				indices[index++] = (short)(t + 3);
			}
		}

		explosionMesh.setVertices(vertices);
		explosionMesh.setIndices(indices);

		explosionModel = ModelBuilder.createFromMesh(explosionMesh, GL20.GL_TRIANGLES, new Material(new BlendingAttribute(
			GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA), TextureAttribute.createDiffuse(explosionTexture)));

		ship = new Ship(shipModel);
		ship.transform.rotate(0, 1, 0, 180);

//		for (int row = 0; row < 4; row++) {
//			for (int column = 0; column < 8; column++) {
//				Invader invader = new Invader(invaderModel, -PLAYFIELD_MAX_X / 2 + column * 2f, 0, PLAYFIELD_MIN_Z + row * 2f);
//				invaders.add(invader);
//			}
//		}

		for (int shield = 0; shield < 3; shield++) {
			blocks.add(new Block(blockModel, -10 + shield * 10 - 1, 0, -2));
			blocks.add(new Block(blockModel, -10 + shield * 10 - 1, 0, -3));
			blocks.add(new Block(blockModel, -10 + shield * 10 + 0, 0, -3));
			blocks.add(new Block(blockModel, -10 + shield * 10 + 1, 0, -3));
			blocks.add(new Block(blockModel, -10 + shield * 10 + 1, 0, -2));
		}
	}

	public void update (float delta) {
        this.levelUpper.update(delta);
        this.canShoot.update(delta);
		ship.update(delta);
		updateInvaders(delta);
		updateShots(delta);
		updateExplosions(delta);
		checkShipCollision();
		checkInvaderCollision();
		checkBlockCollision();
//		checkNextLevel();
	}

    private void addInvader(Class<? extends Invader> invaderClass) {
        float xpos = (float)Math.random() * (PLAYFIELD_MAX_X - PLAYFIELD_MIN_X) + PLAYFIELD_MIN_X;
        Invader invader = this.invaderFactory.get(invaderClass).create(xpos, 0, PLAYFIELD_MIN_Z);
        invaders.add(invader);
    }

	private void updateInvaders (float delta) {

        this.invaderAdder.update(delta);

		for (int i = 0; i < invaders.size(); i++) {
			Invader invader = invaders.get(i);
			invader.update(delta, multiplier);
		}
	}

	private void updateShots (float delta) {
		removedShots.clear();
		for (int i = 0; i < shots.size(); i++) {
			Shot shot = shots.get(i);
			shot.update(delta);
			if (shot.hasLeftField) removedShots.add(shot);
		}

		for (int i = 0; i < removedShots.size(); i++)
			shots.remove(removedShots.get(i));

        for (Shot shipShot : new ArrayList<Shot>(shipShots)) {
            if (shipShot.hasLeftField) shipShots.remove(shipShot);
        }

		if (Math.random() < 0.01 * multiplier && invaders.size() > 0) {
			int index = (int)(Math.random() * (invaders.size() - 1));
			invaders.get(index).transform.getTranslation(tmpV1);
			Shot shot = new Shot(shotModel, tmpV1, true);
			shots.add(shot);
			if (listener != null) listener.shot();
		}
	}

	public void updateExplosions (float delta) {
		removedExplosions.clear();
		for (int i = 0; i < explosions.size(); i++) {
			Explosion explosion = explosions.get(i);
			explosion.update(delta);
            explosion.transform.getTranslation(tmpV1);

			if (explosion.aliveTime > Explosion.EXPLOSION_LIVE_TIME) {
                removedExplosions.add(explosion);
            } else {
                for (Invader invader : new ArrayList<Invader>(invaders)) {
                    invader.transform.getTranslation(tmpV2);
                    if (tmpV1.dst(tmpV2) < invader.getRadius()) {
                        explodeInvader(invader, tmpV2);
                    }
                }
            }
		}

		for (int i = 0; i < removedExplosions.size(); i++)
			explosions.remove(removedExplosions.get(i));
	}

	private void checkInvaderCollision () {

        outer:
		for (int j = 0; j < invaders.size(); j++) {
			Invader invader = invaders.get(j);
			invader.transform.getTranslation(tmpV1);
			if (tmpV1.z > PLAYFIELD_MAX_Z) {
                invaders.remove(invader);
            } else if (tmpV1.x < PLAYFIELD_MIN_X) {
                invaders.remove(invader);
            } else if (tmpV1.x > PLAYFIELD_MAX_X + invader.getRadius()) {
                invaders.remove(invader);
            } else {
                for (Shot shipShot : new ArrayList<Shot> (shipShots)) {
                    shipShot.transform.getTranslation(tmpV2);
                    if (tmpV1.dst(tmpV2) < invader.getRadius()) {
                        shots.remove(shipShot);
                        shipShots.remove(shipShot);
                        explodeInvader(invader, tmpV1);
                        break outer;
                    }
                }
                for (Block block: new ArrayList<Block>(blocks)) {
                    block.transform.getTranslation(tmpV2);
                    if (tmpV1.dst(tmpV2) < Block.BLOCK_RADIUS) {
                        explodeInvader(invader, tmpV1);
                        blocks.remove(block);
                        break;
                    }
                }
            }
		}
	}

    private void explodeInvader(Invader invader, Vector3 location) {
        invaders.remove(invader);
        explosions.add(new Explosion(explosionModel, location));
        if (listener != null) listener.explosion();
        score += invader.getPoints();
    }

	private void checkShipCollision () {
		removedShots.clear();

		if (!ship.isExploding) {
			ship.transform.getTranslation(tmpV1);
			for (int i = 0; i < shots.size(); i++) {
				Shot shot = shots.get(i);
				if (!shot.isInvaderShot) continue;
				shot.transform.getTranslation(tmpV2);
				if (tmpV1.dst(tmpV2) < Ship.SHIP_RADIUS) {
					removedShots.add(shot);
					shot.hasLeftField = true;
					ship.lives--;
					ship.isExploding = true;
					explosions.add(new Explosion(explosionModel, tmpV1));
					if (listener != null) listener.explosion();
					break;
				}
			}

			for (int i = 0; i < removedShots.size(); i++)
				shots.remove(removedShots.get(i));
		}

		ship.transform.getTranslation(tmpV2);
		for (int i = 0; i < invaders.size(); i++) {
			Invader invader = invaders.get(i);
			invader.transform.getTranslation(tmpV1);
			if (tmpV1.dst(tmpV2) < Ship.SHIP_RADIUS) {
				ship.lives--;
				invaders.remove(invader);
				ship.isExploding = true;
				explosions.add(new Explosion(explosionModel, tmpV1));
				explosions.add(new Explosion(explosionModel, tmpV2));
				if (listener != null) listener.explosion();
				break;
			}
		}
	}

	private void checkBlockCollision () {
		removedShots.clear();

		for (int i = 0; i < shots.size(); i++) {
			Shot shot = shots.get(i);
			shot.transform.getTranslation(tmpV2);

			for (int j = 0; j < blocks.size(); j++) {
				Block block = blocks.get(j);
				block.transform.getTranslation(tmpV1);
				if (tmpV1.dst(tmpV2) < Block.BLOCK_RADIUS) {
					removedShots.add(shot);
					shot.hasLeftField = true;
					blocks.remove(block);
					break;
				}
			}
		}

		for (int i = 0; i < removedShots.size(); i++)
			shots.remove(removedShots.get(i));
	}

	public void moveShipUp (float delta, float scale) {
		if (ship.isExploding) return;

		ship.transform.trn(0, 0, -delta * Ship.SHIP_Z_VELOCITY * scale);
		ship.transform.getTranslation(tmpV1);
		if (tmpV1.z < PLAYFIELD_MIN_Z) ship.transform.trn(0, 0, PLAYFIELD_MIN_Z - tmpV1.z);
	}

    public void moveShipDown (float delta, float scale) {
        if (ship.isExploding) return;

        ship.transform.trn(0, 0, +delta * Ship.SHIP_Z_VELOCITY * scale);
        ship.transform.getTranslation(tmpV1);
        if (tmpV1.z > PLAYFIELD_MAX_Z - Ship.SHIP_RADIUS * 2) ship.transform.trn(0, 0, PLAYFIELD_MAX_Z - tmpV1.z - Ship.SHIP_RADIUS * 2);
    }

    public void moveShipLeft (float delta, float scale) {
        if (ship.isExploding) return;

        ship.transform.trn(-delta * Ship.SHIP_VELOCITY * scale, 0, 0);
        ship.transform.getTranslation(tmpV1);

        if (tmpV1.x < PLAYFIELD_MIN_X) ship.transform.trn(PLAYFIELD_MIN_X - tmpV1.x, 0, 0);

        int rotationAmount = ((int)scale * 2 + Ship.SHIP_ROTATION);
        int rotation = ship.rotation - rotationAmount;
        if (rotation > -Ship.SHIP_MAX_ROTATION) {
            ship.rotation = rotation;
            ship.transform.rotate(Vector3.Z, -rotationAmount);
        }
    }

    public void notMovingLeftOrRight() {
        if (ship.rotation != 0) {
            int delta_mod = (ship.rotation > 0 ? -1 : 1);
            ship.rotation += Ship.SHIP_ROTATION * delta_mod;
            ship.transform.rotate(Vector3.Z, Ship.SHIP_ROTATION * delta_mod);
        }
    }

	public void moveShipRight (float delta, float scale) {
		if (ship.isExploding) return;

		ship.transform.trn(+delta * Ship.SHIP_VELOCITY * scale, 0, 0);
		ship.transform.getTranslation(tmpV1);
		if (tmpV1.x > PLAYFIELD_MAX_X) ship.transform.trn(PLAYFIELD_MAX_X - tmpV1.x, 0, 0);

        int rotationAmount = ((int)scale * 2 + Ship.SHIP_ROTATION);
        int rotation = ship.rotation + rotationAmount;
        if (rotation < Ship.SHIP_MAX_ROTATION) {
            ship.rotation = rotation;
            ship.transform.rotate(Vector3.Z, rotationAmount);
        }
	}

	public void shot () {
        if (!ship.isExploding && this.canShoot.trigger()) {
            ship.transform.getTranslation(tmpV1);
            Shot shipShot = new Shot(shotModel, tmpV1, false);
            shipShots.add(shipShot);
            shots.add(shipShot);
            if (listener != null) listener.shot();
        }
	}

	@Override
	public void dispose () {
		shipModel.dispose();
		blockModel.dispose();
		shotModel.dispose();
		explosionModel.dispose();
	}
}
