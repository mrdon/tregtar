package com.badlogic.invaders.simulation;

import com.badlogic.gdx.assets.AssetManager;

/**
 * Created by mrdon on 1/1/15.
 */
public interface InvaderFactory<T extends Invader> {

    void loadAssets(AssetManager assetManager);

    void doneLoading();

    T create(float x, float y, float z);

    void dispose();
}
