/*
 * Copyright 2010 Mario Zechner (contact@badlogicgames.com), Nathan Sweet (admin@esotericsoftware.com)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package com.badlogic.invaders.simulation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;

public class UfoInvader extends Invader {
	public static float INVADER_ROTATION = 45f;

	public UfoInvader(Model model, float x, float y, float z) {
		super(model, x, y, z);
	}

    public float getRadius() {
        return 0.75f;
    }

    @Override
    public float getPoints() {
        return 40;
    }

    public void update (float delta, float speedMultiplier) {
        super.update(delta, speedMultiplier);

        float x = (float) Math.sin(time) * 0.06f;
        float z = time * 0.02f * speedMultiplier;
        transform.trn(x, 0, z);

		transform.rotate(0, 1, 0, INVADER_ROTATION * delta);
	}

    public static class Factory implements InvaderFactory<UfoInvader> {

        private Model invaderModel;
        private AssetManager assetManager;

        @Override
        public void loadAssets(AssetManager assetManager) {
            assetManager.load("data/invader.obj", Model.class);
            this.assetManager = assetManager;
        }

        @Override
        public void doneLoading() {
            invaderModel = assetManager.get("data/invader.obj");
            final Texture invaderTexture = new Texture(Gdx.files.internal("data/invader.png"),
                    Pixmap.Format.RGB565, true);
            invaderTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.Linear);
            invaderModel.materials.get(0).set(TextureAttribute.createDiffuse(invaderTexture));
        }

        @Override
        public UfoInvader create(float x, float y, float z) {
            return new UfoInvader(invaderModel, x, y, z);
        }

        @Override
        public void dispose() {
            invaderModel.dispose();
        }
    }
}
