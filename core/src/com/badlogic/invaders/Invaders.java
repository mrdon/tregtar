/*
 * Copyright 2010 Mario Zechner (contact@badlogicgames.com), Nathan Sweet (admin@esotericsoftware.com)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package com.badlogic.invaders;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.utils.Array;
import com.badlogic.invaders.screens.ExitScreen;
import com.badlogic.invaders.screens.GameLoop;
import com.badlogic.invaders.screens.GameOver;
import com.badlogic.invaders.screens.InvadersScreen;
import com.badlogic.invaders.screens.MainMenu;
import com.badlogic.invaders.simulation.Invader;
import com.badlogic.invaders.simulation.InvaderFactory;
import com.badlogic.invaders.simulation.PokeyInvader;
import com.badlogic.invaders.simulation.UfoInvader;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class Invaders extends Game {

	/** Music needs to be a class property to prevent being disposed. */
	private Music music;
	private FPSLogger fps;
    private long highScore;
    private long currentScore;
    private boolean loading = false;

	private Controller controller;
    private final Map<Class<? extends Invader>, InvaderFactory<?>> invaderFactories =
            new HashMap<Class<? extends Invader>, InvaderFactory<?>>();
	private ControllerAdapter controllerListener = new ControllerAdapter(){
		@Override
		public void connected(Controller c) {
			if (controller == null) {
				controller = c;
			}
		}
		@Override
		public void disconnected(Controller c) {
			if (controller == c) {
				controller = null;
			}
		}
	};
    private AssetManager assetManager;

    public Controller getController() {
		return controller;
	}

	@Override
	public void render () {
        if (loading && assetManager.update())
            doneLoading();
		InvadersScreen currentScreen = getScreen();

		// update the screen
		currentScreen.render(Gdx.graphics.getDeltaTime());

        if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
            exit();
        }

		// When the screen is done we change to the
		// next screen. Ideally the screen transitions are handled
		// in the screen itself or in a proper state machine.
        Class<? extends InvadersScreen> next = currentScreen.next();
		if (next != null) {
			// dispose the resources of the current screen
			currentScreen.dispose();
            if (next == ExitScreen.class) {
                exit();
            } else {
                InvadersScreen nextScreen = createScreen(next);
                setScreen(nextScreen);
            }
		}

		fps.log();
	}

    private void doneLoading() {
        for (InvaderFactory factory : invaderFactories.values()) {
            factory.doneLoading();
        }
    }

    private void exit() {
        music.dispose();
        for (InvaderFactory factory : invaderFactories.values()) {
            factory.dispose();
        }
        System.exit(0);
    }

    private InvadersScreen createScreen(Class<? extends InvadersScreen> next) {
        try {
            return next.getConstructor(Invaders.class).newInstance(this);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean updateHighScore(long score) {
        currentScore = score;
        if (score > highScore) {
            highScore = score;
            return true;
        }
        return false;
    }

    public long getCurrentScore() {
        return currentScore;
    }


    public long getHighScore() {
        return highScore;
    }

    @Override
	public void create () {
        assetManager = new AssetManager();

        invaderFactories.put(UfoInvader.class, new UfoInvader.Factory());
        invaderFactories.put(PokeyInvader.class, new PokeyInvader.Factory());

        for (InvaderFactory factory : invaderFactories.values()) {
            factory.loadAssets(assetManager);
        }

		Array<Controller> controllers = Controllers.getControllers();
		if (controllers.size > 0) {
			controller = controllers.first();
		}
		Controllers.addListener(controllerListener);

		setScreen(new MainMenu(this));
		music = Gdx.audio.newMusic(Gdx.files.getFileHandle("data/8.12.mp3", FileType.Internal));
		music.setLooping(true);
		//music.play();
		Gdx.input.setInputProcessor(new InputAdapter() {
			@Override
			public boolean keyUp (int keycode) {
				if (keycode == Keys.ENTER && Gdx.app.getType() == ApplicationType.WebGL) {
					if (!Gdx.graphics.isFullscreen()) Gdx.graphics.setDisplayMode(Gdx.graphics.getDisplayModes()[0]);
				}
				return true;
			}
		});

		fps = new FPSLogger();

        loading = true;
	}

	/** For this game each of our screens is an instance of InvadersScreen.
	 * @return the currently active {@link InvadersScreen}. */
	@Override
	public InvadersScreen getScreen () {
		return (InvadersScreen)super.getScreen();
	}

    public Map<Class<? extends Invader>, InvaderFactory<?>> getInvaderFactories() {
        return invaderFactories;
    }
}
