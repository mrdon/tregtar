package com.badlogic.invaders;

import java.util.concurrent.TimeUnit;

public class NoMoreThan {

    private float time;
    private final int period;
    private final TimeUnit unit;

    public NoMoreThan(int period, TimeUnit unit) {

        this.period = period;
        this.unit = unit;
    }

    public void update(float delta) {
        this.time += delta;
    }

    public boolean trigger() {
        long timeInMillis = (int)(this.time * 1000);
        if (timeInMillis > this.unit.toMillis(period)) {
            this.time = 0;
            return true;
        }
        return false;
    }
}
