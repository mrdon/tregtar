package com.badlogic.invaders;

import java.util.concurrent.TimeUnit;

public class EveryXTime {

    private float time;
    private final int period;
    private final TimeUnit unit;
    private final Runnable runnable;

    public EveryXTime(int period, TimeUnit unit, Runnable runnable) {

        this.period = period;
        this.unit = unit;
        this.runnable = runnable;
    }

    public void update(float delta) {
        this.time += delta;

        long timeInMillis = (int)(this.time * 1000);
        if (timeInMillis > this.unit.toMillis(period)) {
            runnable.run();
            this.time = 0;
        }
    }
}
